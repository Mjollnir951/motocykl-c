﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Motocykl
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        private void refresh_Click(object sender, EventArgs e)
        {
            Form1_Load(refresh, e);
        }

        private void Dodaj_Click(object sender, EventArgs e)
        {
            Dodaj_motocykl add_m = new Dodaj_motocykl();
            add_m.Show();
            
        }

        private void Wybor_Click(object sender, EventArgs e)
        {
            if(Zarzadzanie.isEmpty() == true)
            {
                MessageBox.Show("W garażu nie ma żadnego motocykla ! Dodaj go.");
                return;
            }
            else
            {
                Wybor choose = new Wybor();
                choose.Show();
            }
        }

        private void Tankowanie_Click(object sender, EventArgs e)
        {
            if (Zarzadzanie.isEmpty() == true)
            {
                MessageBox.Show("W garażu nie ma żadnego motocykla ! Dodaj go.");
                return;
            }
            else
            {
                
                Dodaj_tankowanie add_pal = new Dodaj_tankowanie();
                add_pal.Show();
            }
            
        }

        private void Spalanie_Click(object sender, EventArgs e)
        {
            if (Zarzadzanie.isEmpty() == true)
            {
                MessageBox.Show("W garażu nie ma żadnego motocykla ! Dodaj go.");
                return;
            }
            else
            {
                Spalanie Spal = new Spalanie();
                Spal.Show();
            }
            
        }

        private void Wymiana_Click(object sender, EventArgs e)
        {
            if (Zarzadzanie.isEmpty() == true)
            {
                MessageBox.Show("W garażu nie ma żadnego motocykla ! Dodaj go.");
                return;
            }
            else
            {
                Button przycisk = (sender as Button);

                if (przycisk.Name == Wymiana_oleju.Name)
                {
                    Kilometry_olej kmo = new Kilometry_olej();
                    kmo.Show();
                }
                else if (przycisk.Name == Wymiana_klockow.Name)
                {
                    Kilometry_klocki kmk = new Kilometry_klocki();
                    kmk.Show();
                }
                else if (przycisk.Name == Wymiana_1.Name)
                {
                    Kilometry_w1 kw1 = new Kilometry_w1();
                    kw1.Show();
                }
                else if (przycisk.Name == Wymiana_2.Name)
                {
                    Kilometry_w2 kw2 = new Kilometry_w2();
                    kw2.Show();
                }
            }
            
        }

        private void Wyjście_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e) // Ładujemy tutaj główny formularz
        {
            pictureBox1.Image = Properties.Resources.Poczatkowy; 
            if (Zarzadzanie.isEmpty()==false) // tylko jeśli lista motocykli nie jest pusta 
            {
                Model.Text = Zarzadzanie.getNazwa(Zarzadzanie.Selected);
                Mileage.Text = Zarzadzanie.getPrzebieg().ToString();
                current.Text = Zarzadzanie.getCurrent().ToString();

                if (Zarzadzanie.edytuj(1) >= 0) // Jesli stan oleju nie jest ujemny to ustawiamy
                {
                    Oil_bar.Value = Zarzadzanie.edytuj(1);
                    // Jesli jest ponad 100 to metoda stan_oleju i tak zwroci 100 więc kwalifikuje się do wymiany
                    if (Zarzadzanie.edytuj(1) == 100) MessageBox.Show("Należy niezwłocznie wymienić olej !");  
                }
                else MessageBox.Show("Błędna wartość dla stanu zużycia oleju!");

                if (Zarzadzanie.edytuj(3) >= 0)
                {
                    Brakepads_bar.Value = Zarzadzanie.edytuj(3);
                    if (Zarzadzanie.edytuj(3) == 100) MessageBox.Show("Należy niezwłocznie wymienić klocki !");
                }
                else MessageBox.Show("Błędna wartość dla stanu zużycia klocków!");

                // Tutaj już wartości zaczynają zależeć od typu wybranego motocykla

                if (Zarzadzanie.getType(Zarzadzanie.Selected) == 1) //Ustawienia dla sportowego
                {
                    Type.Text = "Sportowy";
                    pictureBox1.Image = Properties.Resources.Sportowy;
                    label7.Text = "Stan zużycia opon:";

                    if (Zarzadzanie.edytuj(5) >= 0)
                    {
                        Optional_bar1.Value = Zarzadzanie.edytuj(5);
                        if (Zarzadzanie.edytuj(5) == 100) MessageBox.Show("Należy niezwłocznie wymienić opony !");
                    }
                    else MessageBox.Show("Błędna wartość dla stanu zużycia opon!");

                    label8.Visible = false;
                    Optional_bar2.Visible = false;
                    Wymiana_2.Visible = false;

                }
                else if (Zarzadzanie.getType(Zarzadzanie.Selected) == 2) //Ustawienia dla turystycznego
                {
                    Type.Text = "Turystyczny";
                    pictureBox1.Image = Properties.Resources.Turystyczny;
                    label8.Visible = true;
                    Optional_bar2.Visible = true;
                    Wymiana_2.Visible = true;
                    label7.Text = "Stan zużycia opon:";

                    if (Zarzadzanie.edytuj(5) >= 0)
                    {
                        Optional_bar1.Value = Zarzadzanie.edytuj(5);
                        if (Zarzadzanie.edytuj(5) == 100) MessageBox.Show("Należy niezwłocznie wymienić opony !");
                    }
                    else MessageBox.Show("Błędna wartość dla stanu zużycia opon!");

                    label8.Text = "Stan zużycia łańcucha";

                    if (Zarzadzanie.edytuj(7) >= 0)
                    {
                        Optional_bar2.Value = Zarzadzanie.edytuj(7);
                        if (Zarzadzanie.edytuj(7) == 100) MessageBox.Show("Należy niezwłocznie wymienić łańcuch !");
                    }
                    else MessageBox.Show("Błędna wartość dla stanu zużycia łańcucha!");

                }
                else if (Zarzadzanie.getType(Zarzadzanie.Selected) == 3) //Ustawienia dla terenowego
                {
                    Type.Text = "Terenowy";
                    pictureBox1.Image = Properties.Resources.Terenowy;
                    label8.Visible = true;
                    Optional_bar2.Visible = true;
                    Wymiana_2.Visible = true;
                    label7.Text = "Stan zużycia łańcucha";

                    if (Zarzadzanie.edytuj(5) >= 0)
                    {
                        Optional_bar1.Value = Zarzadzanie.edytuj(5);
                        if (Zarzadzanie.edytuj(5) == 100) MessageBox.Show("Należy niezwłocznie wymienić łańcuch !");
                    }
                    else MessageBox.Show("Błędna wartość dla stanu zużycia łańcucha!");

                    label7.Text = "Stan zużycia filtra paliwa";

                    if (Zarzadzanie.edytuj(7) >= 0)
                    {
                        Optional_bar2.Value = Zarzadzanie.edytuj(7);
                        if (Zarzadzanie.edytuj(7) == 100) MessageBox.Show("Należy niezwłocznie wymienić filtr paliwa !");
                    }
                    else MessageBox.Show("Błędna wartość dla stanu zużycia filtra paliwa!");
                }
            }
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            temp okno = new temp();
            okno.Show();

            
        }

        private void Wczytaj_Click(object sender, EventArgs e)
        {
            DBConnect cn = new DBConnect();

            cn.Read();

            MessageBox.Show("Wczytano motocykle !");
        }
    }
}
