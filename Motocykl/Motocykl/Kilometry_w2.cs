﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Motocykl
{
    public partial class Kilometry_w2 : Form
    {
        public Kilometry_w2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int km = Int32.Parse(textBox1.Text);
            
            if (Zarzadzanie.getType(Zarzadzanie.Selected) == 2) Zarzadzanie.edytuj(10, km);
            else if (Zarzadzanie.getType(Zarzadzanie.Selected) == 3) Zarzadzanie.edytuj(14, km);

            Close();
        }
    }
}
