﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motocykl
{
    public class Sportowy: Motocykl
    {
        private int opony;

        public Sportowy(List<int> km, List<double> pal, string model, int mileage, int oil, int brake, int tires)
            : base(km, pal, model, mileage, oil, brake)
        {
            opony = tires;
        }

        public override int stan_1()
        {
            int stan;
            stan = (int)(((baza_km[baza_km.Count-1] - opony) / 25000f) * 100f);
            if (stan > 100) return 100;
            else return stan;
        }

        public override void wymiana_1(int km)
        {
            opony = km;
        }

        public override int stan_2()
        {
            throw new NotImplementedException();
        }

        public override void wymiana_2(int km)
        {
            throw new NotImplementedException();
        }
    }
}
