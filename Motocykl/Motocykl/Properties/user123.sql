-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2015 at 10:25 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `user123`
--

-- --------------------------------------------------------

--
-- Table structure for table `gas`
--

CREATE TABLE IF NOT EXISTS `gas` (
  `ID` int(11) NOT NULL,
  `PAL1` double unsigned NOT NULL,
  `PAL2` double unsigned NOT NULL,
  `PAL3` double unsigned NOT NULL,
  `PAL4` double unsigned NOT NULL,
  `PAL5` double unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `gas`
--

INSERT INTO `gas` (`ID`, `PAL1`, `PAL2`, `PAL3`, `PAL4`, `PAL5`) VALUES
(1, 8.3, 7.6, 9.2, 0, 0),
(2, 17.3, 9.4, 7.3, 0, 0),
(3, 15.6, 11.2, 6.5, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kilometers`
--

CREATE TABLE IF NOT EXISTS `kilometers` (
  `ID` int(11) NOT NULL,
  `KM1` int(32) NOT NULL,
  `KM2` int(32) NOT NULL,
  `KM3` int(32) NOT NULL,
  `KM4` int(32) NOT NULL,
  `KM5` int(32) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `kilometers`
--

INSERT INTO `kilometers` (`ID`, `KM1`, `KM2`, `KM3`, `KM4`, `KM5`) VALUES
(1, 6600, 12950, 89350, 0, 0),
(2, 6820, 13070, 89480, 0, 0),
(3, 7000, 13220, 89600, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `motorcycles`
--

CREATE TABLE IF NOT EXISTS `motorcycles` (
  `ID` int(11) NOT NULL,
  `Type` int(10) unsigned NOT NULL,
  `Name` text COLLATE utf8_polish_ci NOT NULL,
  `Mileage` int(10) unsigned NOT NULL,
  `Oil` int(10) unsigned NOT NULL,
  `Brake` int(10) unsigned NOT NULL,
  `Prop1` int(10) unsigned NOT NULL,
  `Prop2` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Dumping data for table `motorcycles`
--

INSERT INTO `motorcycles` (`ID`, `Type`, `Name`, `Mileage`, `Oil`, `Brake`, `Prop1`, `Prop2`) VALUES
(1, 1, 'Suzuki GSX 1000 R', 6500, 3650, 4800, 0, 0),
(2, 3, 'KTM 250', 12850, 8320, 7520, 2300, 4960),
(3, 2, 'BMW 1200 GS', 89230, 85420, 88650, 75650, 72540);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gas`
--
ALTER TABLE `gas`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `kilometers`
--
ALTER TABLE `kilometers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `motorcycles`
--
ALTER TABLE `motorcycles`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gas`
--
ALTER TABLE `gas`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kilometers`
--
ALTER TABLE `kilometers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `motorcycles`
--
ALTER TABLE `motorcycles`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
