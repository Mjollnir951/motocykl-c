﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motocykl
{
    public class Terenowy: Motocykl
    {
        private int lancuch, filtr_paliwa;

        public Terenowy(List<int> km, List<double> pal, string model, int mileage, int oil, int brake, int chain, int filter)
            :base(km, pal, model, mileage, oil, brake)
        {
            lancuch = chain;
            filtr_paliwa = filter;
        }

        public override int stan_1()
        {
            int stan;
            stan = (int)(((baza_km[baza_km.Count-1] - lancuch) / 24000f) * 100f);
            if (stan > 100) return 100;
            else return stan;
        }

        public override void wymiana_1(int km)
        {
            lancuch = km;
        }

        public override int stan_2()
        {
            int stan;
            stan = (int)(((baza_km[baza_km.Count-1] - filtr_paliwa) / 10000f) * 100f);
            if (stan > 100) return 100;
            else return stan;
        }

        public override void wymiana_2(int km)
        {
            filtr_paliwa = km;
        }

    }
}
