﻿namespace Motocykl
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Oil_bar = new System.Windows.Forms.ProgressBar();
            this.Spalanie = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.Dodaj = new System.Windows.Forms.Button();
            this.Wybor = new System.Windows.Forms.Button();
            this.Zapis = new System.Windows.Forms.Button();
            this.Tankowanie = new System.Windows.Forms.Button();
            this.Wymiana_oleju = new System.Windows.Forms.Button();
            this.Brakepads_bar = new System.Windows.Forms.ProgressBar();
            this.Optional_bar1 = new System.Windows.Forms.ProgressBar();
            this.Optional_bar2 = new System.Windows.Forms.ProgressBar();
            this.Wymiana_klockow = new System.Windows.Forms.Button();
            this.Wymiana_1 = new System.Windows.Forms.Button();
            this.Wymiana_2 = new System.Windows.Forms.Button();
            this.Model = new System.Windows.Forms.Label();
            this.Type = new System.Windows.Forms.Label();
            this.Mileage = new System.Windows.Forms.Label();
            this.refresh = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.current = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(307, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(227, 162);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Typ motocykla: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Nazwa: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Przebieg początkowy: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Stan zużycia oleju: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 221);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Stan zużycia klocków: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(31, 265);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Stan zużycia";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 309);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Stan zużycia";
            // 
            // Oil_bar
            // 
            this.Oil_bar.Location = new System.Drawing.Point(34, 193);
            this.Oil_bar.Name = "Oil_bar";
            this.Oil_bar.Size = new System.Drawing.Size(306, 25);
            this.Oil_bar.TabIndex = 9;
            // 
            // Spalanie
            // 
            this.Spalanie.Location = new System.Drawing.Point(213, 368);
            this.Spalanie.Name = "Spalanie";
            this.Spalanie.Size = new System.Drawing.Size(136, 30);
            this.Spalanie.TabIndex = 13;
            this.Spalanie.Text = "Spalanie";
            this.Spalanie.UseVisualStyleBackColor = true;
            this.Spalanie.Click += new System.EventHandler(this.Spalanie_Click);
            // 
            // Exit
            // 
            this.Exit.Location = new System.Drawing.Point(387, 413);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(136, 36);
            this.Exit.TabIndex = 14;
            this.Exit.Text = "Wyjście";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Wyjście_Click);
            // 
            // Dodaj
            // 
            this.Dodaj.Location = new System.Drawing.Point(34, 413);
            this.Dodaj.Name = "Dodaj";
            this.Dodaj.Size = new System.Drawing.Size(136, 36);
            this.Dodaj.TabIndex = 15;
            this.Dodaj.Text = "Dodaj motocykl";
            this.Dodaj.UseVisualStyleBackColor = true;
            this.Dodaj.Click += new System.EventHandler(this.Dodaj_Click);
            // 
            // Wybor
            // 
            this.Wybor.Location = new System.Drawing.Point(213, 413);
            this.Wybor.Name = "Wybor";
            this.Wybor.Size = new System.Drawing.Size(136, 36);
            this.Wybor.TabIndex = 16;
            this.Wybor.Text = "Wybierz inny motocykl";
            this.Wybor.UseVisualStyleBackColor = true;
            this.Wybor.Click += new System.EventHandler(this.Wybor_Click);
            // 
            // Zapis
            // 
            this.Zapis.Location = new System.Drawing.Point(387, 368);
            this.Zapis.Name = "Zapis";
            this.Zapis.Size = new System.Drawing.Size(136, 30);
            this.Zapis.TabIndex = 17;
            this.Zapis.Text = "Wczytaj";
            this.Zapis.UseVisualStyleBackColor = true;
            this.Zapis.Click += new System.EventHandler(this.Wczytaj_Click);
            // 
            // Tankowanie
            // 
            this.Tankowanie.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.Tankowanie.Location = new System.Drawing.Point(34, 368);
            this.Tankowanie.Name = "Tankowanie";
            this.Tankowanie.Size = new System.Drawing.Size(136, 30);
            this.Tankowanie.TabIndex = 18;
            this.Tankowanie.Text = "Dodaj tankowanie";
            this.Tankowanie.UseVisualStyleBackColor = true;
            this.Tankowanie.Click += new System.EventHandler(this.Tankowanie_Click);
            // 
            // Wymiana_oleju
            // 
            this.Wymiana_oleju.Location = new System.Drawing.Point(405, 193);
            this.Wymiana_oleju.Name = "Wymiana_oleju";
            this.Wymiana_oleju.Size = new System.Drawing.Size(101, 25);
            this.Wymiana_oleju.TabIndex = 19;
            this.Wymiana_oleju.Text = "Wymień";
            this.Wymiana_oleju.UseVisualStyleBackColor = true;
            this.Wymiana_oleju.Click += new System.EventHandler(this.Wymiana_Click);
            // 
            // Brakepads_bar
            // 
            this.Brakepads_bar.Location = new System.Drawing.Point(34, 237);
            this.Brakepads_bar.Name = "Brakepads_bar";
            this.Brakepads_bar.Size = new System.Drawing.Size(306, 25);
            this.Brakepads_bar.TabIndex = 20;
            // 
            // Optional_bar1
            // 
            this.Optional_bar1.Location = new System.Drawing.Point(34, 281);
            this.Optional_bar1.Name = "Optional_bar1";
            this.Optional_bar1.Size = new System.Drawing.Size(306, 25);
            this.Optional_bar1.TabIndex = 21;
            // 
            // Optional_bar2
            // 
            this.Optional_bar2.Location = new System.Drawing.Point(34, 325);
            this.Optional_bar2.Name = "Optional_bar2";
            this.Optional_bar2.Size = new System.Drawing.Size(306, 25);
            this.Optional_bar2.TabIndex = 22;
            // 
            // Wymiana_klockow
            // 
            this.Wymiana_klockow.Location = new System.Drawing.Point(405, 237);
            this.Wymiana_klockow.Name = "Wymiana_klockow";
            this.Wymiana_klockow.Size = new System.Drawing.Size(101, 25);
            this.Wymiana_klockow.TabIndex = 23;
            this.Wymiana_klockow.Text = "Wymień";
            this.Wymiana_klockow.UseVisualStyleBackColor = true;
            this.Wymiana_klockow.Click += new System.EventHandler(this.Wymiana_Click);
            // 
            // Wymiana_1
            // 
            this.Wymiana_1.Location = new System.Drawing.Point(405, 281);
            this.Wymiana_1.Name = "Wymiana_1";
            this.Wymiana_1.Size = new System.Drawing.Size(101, 25);
            this.Wymiana_1.TabIndex = 24;
            this.Wymiana_1.Text = "Wymień";
            this.Wymiana_1.UseVisualStyleBackColor = true;
            this.Wymiana_1.Click += new System.EventHandler(this.Wymiana_Click);
            // 
            // Wymiana_2
            // 
            this.Wymiana_2.Location = new System.Drawing.Point(405, 325);
            this.Wymiana_2.Name = "Wymiana_2";
            this.Wymiana_2.Size = new System.Drawing.Size(101, 25);
            this.Wymiana_2.TabIndex = 25;
            this.Wymiana_2.Text = "Wymień";
            this.Wymiana_2.UseVisualStyleBackColor = true;
            this.Wymiana_2.Click += new System.EventHandler(this.Wymiana_Click);
            // 
            // Model
            // 
            this.Model.AutoSize = true;
            this.Model.Location = new System.Drawing.Point(38, 35);
            this.Model.Name = "Model";
            this.Model.Size = new System.Drawing.Size(85, 13);
            this.Model.TabIndex = 26;
            this.Model.Text = "..........................";
            // 
            // Type
            // 
            this.Type.AutoSize = true;
            this.Type.Location = new System.Drawing.Point(38, 95);
            this.Type.Name = "Type";
            this.Type.Size = new System.Drawing.Size(76, 13);
            this.Type.TabIndex = 27;
            this.Type.Text = ".......................";
            // 
            // Mileage
            // 
            this.Mileage.AutoSize = true;
            this.Mileage.Location = new System.Drawing.Point(38, 148);
            this.Mileage.Name = "Mileage";
            this.Mileage.Size = new System.Drawing.Size(76, 13);
            this.Mileage.TabIndex = 28;
            this.Mileage.Text = ".......................";
            // 
            // refresh
            // 
            this.refresh.Location = new System.Drawing.Point(185, 65);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(75, 23);
            this.refresh.TabIndex = 29;
            this.refresh.Text = "Odśwież";
            this.refresh.UseVisualStyleBackColor = true;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(182, 122);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Przebieg aktualny:";
            // 
            // current
            // 
            this.current.AutoSize = true;
            this.current.Location = new System.Drawing.Point(182, 148);
            this.current.Name = "current";
            this.current.Size = new System.Drawing.Size(70, 13);
            this.current.TabIndex = 31;
            this.current.Text = ".....................";
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(545, 461);
            this.Controls.Add(this.current);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.refresh);
            this.Controls.Add(this.Model);
            this.Controls.Add(this.Mileage);
            this.Controls.Add(this.Type);
            this.Controls.Add(this.Wymiana_2);
            this.Controls.Add(this.Wymiana_1);
            this.Controls.Add(this.Wymiana_klockow);
            this.Controls.Add(this.Optional_bar2);
            this.Controls.Add(this.Optional_bar1);
            this.Controls.Add(this.Brakepads_bar);
            this.Controls.Add(this.Wymiana_oleju);
            this.Controls.Add(this.Tankowanie);
            this.Controls.Add(this.Zapis);
            this.Controls.Add(this.Wybor);
            this.Controls.Add(this.Dodaj);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.Spalanie);
            this.Controls.Add(this.Oil_bar);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Twój Motocykl";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ProgressBar Oil_bar;
        private System.Windows.Forms.Button Spalanie;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Button Dodaj;
        private System.Windows.Forms.Button Wybor;
        private System.Windows.Forms.Button Zapis;
        private System.Windows.Forms.Button Tankowanie;
        private System.Windows.Forms.Button Wymiana_oleju;
        private System.Windows.Forms.ProgressBar Brakepads_bar;
        private System.Windows.Forms.ProgressBar Optional_bar1;
        private System.Windows.Forms.ProgressBar Optional_bar2;
        private System.Windows.Forms.Button Wymiana_klockow;
        private System.Windows.Forms.Button Wymiana_1;
        private System.Windows.Forms.Button Wymiana_2;
        private System.Windows.Forms.Label Model;
        private System.Windows.Forms.Label Type;
        private System.Windows.Forms.Label Mileage;
        private System.Windows.Forms.Button refresh;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label current;
    }
}

