﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Motocykl
{
    public partial class Kilometry_olej : Form
    {
        public Kilometry_olej()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void button2_Click(object sender, EventArgs e)
        {
            int km = Int32.Parse(textBox1.Text);

            Zarzadzanie.edytuj(2, km);

            Close();
        }
    }
}
