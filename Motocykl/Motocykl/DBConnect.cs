﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient; // dodajemy bibliotekę MySQL

namespace Motocykl
{
    class DBConnect
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string pass;

        public DBConnect()
        {
            Initialize();
        }

        private void Initialize()       //Metoda inicjalizująca parametry połączenia
        {
            server = "localhost";
            database = "user123";
            uid = "user123";
            pass = "123";
            string connect;

            connect = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + pass + ";";

            connection = new MySqlConnection(connect);
        }

        private bool OpenConnection()       //otworzenie połączenia z bazą
        {
            try{
                connection.Open();
                return true;
            }
            catch(MySqlException ex){
                switch (ex.Number)
	            {
                    case 0:
                        MessageBox.Show("Nie można połączyć się z bazą danych! ");
                        break;
                    case 1045:
                        MessageBox.Show("Podany użytkownik/hasło jest błędne! ");
                        break;
	            }
                return false;
            }
        }

        private bool CloseConnection()
        {
            try{
                connection.Close();
                return true;
            }
            catch(MySqlException ex){
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public void Read()
        {
            MySqlConnection conn = null;
            MySqlDataReader rdr1 = null;
            MySqlDataReader rdr2 = null;
            MySqlDataReader rdr3 = null;

            try
            {
                //////////////////////// połączenie z pierwszą tabelą 
                string connect1;
                connect1 = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + pass + ";";
                conn = new MySqlConnection(connect1);
                conn.Open();

                string stm1 = "SELECT * FROM motorcycles";
                MySqlCommand cmd1 = new MySqlCommand(stm1, conn);
                rdr1 = cmd1.ExecuteReader();
                List<int>[] motocykle = new List<int>[3];
                motocykle[0] = new List<int>();
                motocykle[1] = new List<int>();
                motocykle[2] = new List<int>();
                List<string> names = new List<string>();

                while(rdr1.Read())
                {
                    int i = 0;

                    names.Add(rdr1.GetString((i*6)+2));
                    motocykle[i].Add(rdr1.GetInt32((i * 6) + 1));  //typ
                    motocykle[i].Add(rdr1.GetInt32((i * 6) + 3));  //przebieg 
                    motocykle[i].Add(rdr1.GetInt32((i * 6) + 4));  //olej  
                    motocykle[i].Add(rdr1.GetInt32((i * 6) + 5));  //klocki
                    motocykle[i].Add(rdr1.GetInt32((i * 6) + 6));  //wym 1
                    motocykle[i].Add(rdr1.GetInt32((i * 6) + 7));  //wym 2
                    i++;
                }
                conn.Close();
               
                /////////////////////połączenie z drugą tabelą, wybranie 

                string connect2;
                connect2 = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + pass + ";";
                conn = new MySqlConnection(connect2);
                conn.Open();

                string stm2 = "SELECT KM1, KM2, KM3 FROM kilometers";
                MySqlCommand cmd2 = new MySqlCommand(stm2, conn);
                rdr2 = cmd2.ExecuteReader();
                List<int> km = new List<int>();

                while (rdr2.Read())
                {
                        for (int i = 0; i < 3; i++)
                        {
                            km.Add(rdr2.GetInt32(i));
                        }
                }
                conn.Close();

                /////////////////////połączenie z trzecią tabelą

                string connect3;
                connect3 = "SERVER=" + server + ";" + "DATABASE=" + database + ";" + "UID=" + uid + ";" + "PASSWORD=" + pass + ";";
                conn = new MySqlConnection(connect2);
                conn.Open();

                string stm3 = "SELECT PAL1, PAL2, PAL3 FROM gas";
                MySqlCommand cmd3 = new MySqlCommand(stm3, conn);
                rdr3 = cmd3.ExecuteReader();

                List<double> pal = new List<double>();

                while (rdr3.Read())
                {
                    for (int i = 0; i < 3; i++)
                    {
                        pal.Add(rdr3.GetDouble(i));
                    }
                }
                conn.Close();
                //////////////////////// PRZESŁANIE ODEBRANYCH DANYCH DO UTWORZENIA MOTOCYKLI
                for (int i = 0; i < 3; i++)
                {
                    List<int> listkm = new List<int>();
                    List<double> listpal = new List<double>();
                    listpal.Clear();
                    listkm.Clear();
                    int typ = 0;
                    int przebieg = 0;
                    int olej = 0;
                    int klocki = 0;
                    int wym1 = 0;
                    int wym2 = 0;
                    string nazwa = "";


                    typ = motocykle[0][i * 6];
                    przebieg = motocykle[0][(i * 6) + 1];
                    olej = motocykle[0][(i * 6) + 2];
                    klocki = motocykle[0][(i * 6) + 3];
                    wym1 = motocykle[0][(i * 6) + 4];
                    wym2 = motocykle[0][(i * 6) + 5];
                    nazwa = names[i];

                    listkm.Add(przebieg);
                    listkm.Add(km[0+i]);
                    listkm.Add(km[3+i]);
                    listkm.Add(km[6+i]);

                    listpal.Add(pal[0 + i]);
                    listpal.Add(pal[3 + i]);
                    listpal.Add(pal[6 + i]);

                    Zarzadzanie.add(listkm, listpal, typ, nazwa, przebieg, olej, klocki, wym1, wym2);
                }

            }catch(MySqlException ex)
            {
                MessageBox.Show("Error:" + ex.ToString());
            }finally
            {
                if (rdr1!= null)
                {
                    rdr1.Close();
                }

                if (conn != null)
                {
                    conn.Close();
                }
            }
        }
    }
}
