﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Motocykl
{
    public partial class Spalanie : Form
    {
        public Spalanie()
        {
            InitializeComponent();
        }

        private void Spalanie_Load(object sender, EventArgs e)
        {
            this.Size = new Size(390,320);
            ostatnie.Text = Zarzadzanie.getSpalanie().ToString();
            srednie.Text = Zarzadzanie.getSpalanie_srednie().ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Size = new Size(390, 690);

            listBox1.Visible = true;
            listBox2.Visible = true;
            listBox2.Items.Add("1. ---------");
            List<int> kms = Zarzadzanie.getKilometry();
            List<double> pal = Zarzadzanie.getPaliwo();

            int i=0;
            for ( i = 0; i < pal.Count(); i++)
            {
                int lp = i + 1;
                listBox1.Items.Add((i+1).ToString() + ". " + kms[i].ToString());
                listBox2.Items.Add((i+2).ToString() + ". " + pal[i].ToString());
            }
            listBox1.Items.Add((i + 1).ToString() + ". " + kms[i].ToString());

        }
    }
}
