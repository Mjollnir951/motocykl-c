﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Motocykl
{
    public partial class Dodaj_motocykl : Form
    {
        public Dodaj_motocykl()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Visible = false;
            label2.Visible = true;
            name_box.Visible = true;
            label3.Visible = true;
            mileage_box.Visible = true;
            label4.Visible = true;
            oil_box.Visible = true;
            label5.Visible = true;
            brake_box.Visible = true;

            int typ = Int32.Parse(type_box.Text);
            
            if (typ == 1)
            {
                label6.Visible = true;
                change1_box.Visible = true;
                label6.Text = "Ostatnia wymiana opon przy:";
            }
            else if (typ == 2)
            {
                label6.Visible = true;
                change1_box.Visible = true;
                label6.Text = "Ostatnia wymiana łańcucha przy:";
                label7.Visible = true;
                change2_box.Visible = true;
                label7.Text = "Ostatnia wymiana opon przy:";
            }
            else if (typ == 3)
            {
                label6.Visible = true;
                change1_box.Visible = true;
                label6.Text = "Ostatnia wymiana łańcucha przy:";
                label7.Visible = true;
                change2_box.Visible = true;
                label7.Text = "Ostatnia wymiana filtra paliwa przy:";
            }
        }

        private void Add_Click(object sender, EventArgs e)
        {
            int typ = Int32.Parse(type_box.Text);
            string nazwa = name_box.Text;
            int przebieg_p = Int32.Parse(mileage_box.Text);
            int olej = Int32.Parse(oil_box.Text);
            int klocki = Int32.Parse(brake_box.Text);
            int wymiana1 = Int32.Parse(change1_box.Text);
            int wymiana2 = 0;
            if(change2_box.Visible==true)
            {
                wymiana2 = Int32.Parse(change2_box.Text);
            }

            List<int> km = new List<int>();
            List<double> pal = new List<double>();
            km.Add(przebieg_p);
            Zarzadzanie.add(km, pal, typ, nazwa, przebieg_p, olej, klocki, wymiana1, wymiana2);
            Close();
            
        }

        
    }
}
