﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Motocykl
{
    public partial class Wybor : Form
    {
        public Wybor()
        {
            InitializeComponent();
        }

        private void Wybor_Load(object sender, EventArgs  e)
        {
            label1.Text = Zarzadzanie.getNazwa(0);
            
            if (Zarzadzanie.get_garagesize() == 2)
            {
                button2.Visible = true;
                label2.Visible = true;
                label2.Text = Zarzadzanie.getNazwa(1);
            }
            else if (Zarzadzanie.get_garagesize() == 3)
            {
                button2.Visible = true;
                label2.Visible = true;
                label2.Text = Zarzadzanie.getNazwa(1);
                button3.Visible = true;
                label3.Visible = true;
                label3.Text = Zarzadzanie.getNazwa(2);
            }
            else if (Zarzadzanie.get_garagesize() == 4)
            {
                button2.Visible = true;
                label2.Visible = true;
                label2.Text = Zarzadzanie.getNazwa(1);
                button3.Visible = true;
                label3.Visible = true;
                label3.Text = Zarzadzanie.getNazwa(2);
                button4.Visible = true;
                label4.Visible = true;
                label4.Text = Zarzadzanie.getNazwa(3);
            }
            else if (Zarzadzanie.get_garagesize() == 5)
            {
                button2.Visible = true;
                label2.Visible = true;
                label2.Text = Zarzadzanie.getNazwa(1);
                button3.Visible = true;
                label3.Visible = true;
                label3.Text = Zarzadzanie.getNazwa(2);
                button4.Visible = true;
                label4.Visible = true;
                label4.Text = Zarzadzanie.getNazwa(3);
                button5.Visible = true;
                label5.Visible = true;
                label5.Text = Zarzadzanie.getNazwa(4);
            }
             
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Zarzadzanie.Selected = 0;
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Zarzadzanie.Selected = 1;
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Zarzadzanie.Selected = 2;
            Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Zarzadzanie.Selected = 3;
            Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Zarzadzanie.Selected = 4;
            Close();
        }



        
    }
}
