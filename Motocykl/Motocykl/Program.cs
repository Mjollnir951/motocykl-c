﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Motocykl
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MessageBox.Show("Program w tej wersji wymaga uruchomienia lokalnie bazy danych MySql, jej pliki znajdują się w katalogu 'Properties'! ");
            Application.Run(new Form1());
            
            
        }
    }
}
