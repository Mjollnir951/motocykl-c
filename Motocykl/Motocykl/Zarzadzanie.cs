﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Motocykl
{
    public sealed class Zarzadzanie     //Klasa wzorca projektowego Singleton 
    {
        private static Zarzadzanie instance;
        private Zarzadzanie() { }
        private static int selected = 0;
        private static List<Motocykl> garage = new List<Motocykl>();

        public static Zarzadzanie Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new Zarzadzanie();
                }
                return instance;
            }
        }

        public static int Selected
        {
            get { return selected; }
            set { selected = value; }
        }

        public static int getPrzebieg()
        {
            return garage[selected].Przebieg;
        }

        public static List<int> getKilometry()
        {
            return garage[selected].Kilometry;
        }

        public static List<double> getPaliwo()
        {
            return garage[selected].Paliwo;
        }

        public static double getSpalanie()
        {
            garage[selected].wylicz_spalanie();
            return garage[selected].Spalanie;
        }

        public static double getSpalanie_srednie()
        {
            return garage[selected].Spalanie_srednie;
        }

        public static int getCurrent()
        {
            return garage[selected].Baza_km;
        }

        public static bool isEmpty()
        {
            if (garage.Count == 0)  return true;
            else return false;
        }

        public static string getNazwa(int numer)
        {
            return garage[numer].Nazwa;
        }

        public static int getType(int x)
        {
            if (garage[x] is Sportowy) return 1;
            else if (garage[x] is Turystyczny) return 2;
            else if (garage[x] is Terenowy) return 3;
         
            return 0;
        }

        public static void add(List<int> km, List<double> pal, int type, string model, int mileage, int oil, int brake, int change1,int change2)
        {
            if(type == 1)
            {
                garage.Add(new Sportowy(km, pal, model, mileage, oil, brake, change1));
            }
            else if (type == 2)
            {
                garage.Add(new Turystyczny(km, pal, model, mileage, oil, brake, change1, change2));
            }
            else if (type == 3)
            {
                garage.Add(new Terenowy(km, pal, model, mileage, oil, brake, change1, change2));
            }
        }

        public static int edytuj(int wybor, int km = 0)
        {
            switch (wybor)
	        {
                case 1:
                    return garage[selected].stan_oleju();
                case 2:
                    garage[selected].wymiana_oleju(km);
                    return 0;
                case 3:
                    return garage[selected].stan_klockow();
                case 4:
                    garage[selected].wymiana_klockow(km);
                    return 0;
                case 5:
                    return garage[selected].stan_1();
                case 6:
                    garage[selected].wymiana_1(km);
                    return 0;
                case 7:
                    return garage[selected].stan_2();
                case 8:
                    garage[selected].wymiana_2(km);
                    return 0;
		        default:
                    return 0;
	        }
        }

        public static void tankowanie(int km, double pal)
        {
            garage[selected].dodaj_km(km, pal);
        }

        public static int get_garagesize()
        {
            int size = garage.Count;
            return size;
        }
        
    }
}
