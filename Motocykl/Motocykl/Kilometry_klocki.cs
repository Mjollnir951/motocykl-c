﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Motocykl
{
    public partial class Kilometry_klocki : Form
    {
        public Kilometry_klocki()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int km = Int32.Parse(textBox1.Text);

            Zarzadzanie.edytuj(4, km);
            Close();
        }
    }
}
