﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Motocykl
{
    public partial class Dodaj_tankowanie : Form
    {
        public Dodaj_tankowanie()
        {
            InitializeComponent();
        }

        private void Quit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            int km = Int32.Parse(textBox1.Text);
            double pal = Double.Parse(textBox2.Text);
            Zarzadzanie.tankowanie(km, pal);

            Close();

        }
    }
}
