﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motocykl 
{
    public abstract class Motocykl
    {
        protected string nazwa;
        protected int olej, klocki, przebieg_pocz;
        protected double spalanie;
        protected double spalanie_srednie;
        protected List<int> baza_km;
        protected List<double> baza_pal;


        protected Motocykl(List<int> km, List<double> pal ,string model, int mileage, int oil, int brake)
        {
            baza_km = km;
            baza_pal = pal;
            nazwa = model;
            przebieg_pocz = mileage;
            olej = oil; 
            klocki = brake; 
        }

        public string Nazwa
        {
            get { return nazwa; }
        }

        public int Przebieg
        {
            get { return przebieg_pocz; }
        }

        public List<int> Kilometry
        {
            get { return baza_km; }
        }

        public List<double> Paliwo
        {
            get { return baza_pal; }
        }

        public double Spalanie
        {
            get { return spalanie; }
        }

        public double Spalanie_srednie
        {
            get { return spalanie_srednie; }
        }

        public int Baza_km
        {
            get { return baza_km[baza_km.Count - 1]; }
        }

        public int stan_oleju()
        {
            int stan;
            stan = (int)(((baza_km[baza_km.Count-1] - olej) / 6000f) * 100f);
            if (stan > 100) return 100;
            else return stan;
            
        }

        public void wymiana_oleju(int km)
        {
            olej = km;

        }

        public int stan_klockow()
        {
            int stan;
            stan = (int)(((baza_km[baza_km.Count-1] - klocki) / 20000f) * 100f);
            if (stan > 100) return 100;
            else return stan;
        }

        public void wymiana_klockow(int km)
        {
            klocki = km;
        }

        public void wylicz_spalanie()
        {
            double km = baza_km[baza_km.Count-1] - baza_km[baza_km.Count - 2];
            spalanie = (100f * baza_pal[baza_pal.Count-1])/km;

            double paliwo=0;
            double przebieg = baza_km[baza_km.Count-1] - baza_km[0];
            for (int i = 0; i < baza_pal.Count; i++)
			{
			    paliwo += baza_pal[i];
			}
            spalanie_srednie = (100f * paliwo )/przebieg;
        }

        public void dodaj_km(int km, double pal)
        {
            baza_km.Add(km);
            baza_pal.Add(pal);
        }

        public abstract int stan_1();

        public abstract void wymiana_1(int km);

        public abstract int stan_2();

        public abstract void wymiana_2(int km);

    }
}
